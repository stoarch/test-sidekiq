require 'sidekiq'
require 'timeout'

Sidekiq.configure_client do |config|
  config.redis = {db:1}
end

Sidekiq.configure_server do |config|
  config.redis = {db:1}
end

FILE_NAME = "data.txt"

def is_file_locked?(file)
  f = File.open(file, File::CREAT)
  Timeout::timeout(0.001){ f.flock(File::LOCK_EX)}
  f.flock(File::LOCK_UN)
  false
rescue 
  true
ensure
  f.close
end

class WriterWorker 
  include Sidekiq::Worker

  attr :fh

  def perform(value)
    puts "Writing value #{value} "
    sleep 10 while is_file_locked?(FILE_NAME)

    @fh = File.open(FILE_NAME,"a")
    @fh.flock(File::LOCK_EX)
    @fh.puts(value)
    @fh.flock(File::LOCK_UN)

    puts "Value #{value} written"
  ensure
    @fh.flock(File::LOCK_UN) unless @fh.nil?
    @fh.close unless @fh.nil?
  end
end
