#!/usr/bin/env ruby
# Main ruby app for launching storing apps
#
require_relative 'writer_worker'

fh = File.open("data.txt",File::CREAT)
fh.close

10.times do |i|
  WriterWorker.perform_async(i)
end

puts "Done"

